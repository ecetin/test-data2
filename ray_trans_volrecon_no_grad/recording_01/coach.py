import torch
from easydict import EasyDict as edict
import os
from torch.utils.data import DataLoader
import time
import tqdm
import imageio
from collections import OrderedDict
import socket
import numpy as np
import re
import math

import misc
import datasets
import models
import options

from misc.utils import log
from misc.metrics import EvalTools
from misc.train_helpers import summarize_metrics, summarize_loss
from datasets import datas_dict
from models import models_dict
from misc import utils


class Coach():
    def __init__(self, opts):
        super().__init__()
        self.opts = opts
        self.n_src_views = opts.n_src_views
        self.epoch_start = 0
        self.iter_start =0
        os.makedirs(opts.output_path, exist_ok=True)
        if hasattr(self.opts, "telegram") and self.opts.telegram.activate:
            from misc.train_helpers import TGDebugMessager
            self.tg_trainnet_bot = TGDebugMessager(self.opts.telegram.token, self.opts.telegram.chat_id)

    def L1_loss(self, pred, label=0):
        loss = (pred.contiguous() - label).abs()
        return loss.mean()

    def MSE_loss(self, pred, label=0):
        loss = (pred.contiguous() - label) ** 2
        return loss.mean()

    def load_dataset(self, splits):
        # load training data
        log.info(f"loading datasets...")
        for split in splits:
            if getattr(self.opts, f'data_{split}', None):
                if split == 'test':
                    data_opts_list = [v for _, v in self.opts.data_test.items()]
                    self.test_loaders = []
                else:
                    data_opts_list = [getattr(self.opts, f'data_{split}')]

                for data_opts in data_opts_list:
                    if data_opts is None:
                        continue
                    scene_list = getattr(data_opts, "scene_list", None)
                    test_views_method = getattr(data_opts, "test_views_method", "nearest")
                    cur_dataset = datas_dict[data_opts.dataset_name](data_opts.root_dir, split,
                                            n_views=self.n_src_views, img_wh=data_opts.img_wh, max_len=data_opts.max_len,
                                            scene_list=scene_list, test_views_method=test_views_method)
                    cur_loader = DataLoader(cur_dataset, shuffle=(split == 'train'), num_workers=data_opts.num_workers,
                                            batch_size=self.opts.batch_size, pin_memory=True)
                    if split == 'test':
                        self.test_loaders.append(cur_loader)
                    else:
                        setattr(self, f"{split}_loader", cur_loader)
                    log.info(f"  * loaded {split} set of {data_opts.dataset_name}")

    def build_networks(self):
        log.info("building networks...")
        self.model = models_dict[self.opts.model](self.opts).to(self.opts.device)
        if self.opts.encoder.pretrain_weight and (not self.opts.load) and (not self.opts.resume):
            utils.load_gmflow_checkpoint(self.model.feat_enc, self.opts.encoder.pretrain_weight, self.opts.device,
                                    gmflow_n_blocks=self.opts.encoder.num_transformer_layers)
            log.info(f"loaded gmflow pretrained weight for encoder from {self.opts.encoder.pretrain_weight}.")

        if len(self.opts.gpu_ids) > 1:  # Use multi GPU training
            self.model.feat_enc = torch.nn.DataParallel(self.model.feat_enc, self.opts.gpu_ids)
            self.model.nerf_dec = torch.nn.DataParallel(self.model.nerf_dec, self.opts.gpu_ids)
            if hasattr(self.model, "nerf_dec_fine"):
                self.model.nerf_dec_fine = torch.nn.DataParallel(self.model.nerf_dec_fine, self.opts.gpu_ids)

    def setup_optimizer(self):
        log.info("setting up optimizers...")
        # load trainable params
        optim_params = [dict(params=self.model.feat_enc.parameters(), lr=self.opts.optim.lr_enc),
                        dict(params=self.model.nerf_dec.parameters(), lr=self.opts.optim.lr_dec)]
        lr_lists = [self.opts.optim.lr_enc, self.opts.optim.lr_dec]
        if self.opts.nerf.fine_sampling:
            optim_params.append(dict(params=self.model.nerf_dec_fine.parameters(), lr=self.opts.optim.lr_dec))
            lr_lists.append(self.opts.optim.lr_dec)

        # set up optimizer
        optim_type = self.opts.optim.algo.type
        optim_kwargs = {k:v for k,v in self.opts.optim.algo.items() if k != "type"}
        self.optim = getattr(torch.optim, optim_type)(optim_params, **optim_kwargs)
        info = f"  * {optim_type} optimizer (" + ', '.join([f'{k}={v}' for k, v in optim_kwargs.items()]) + ')'
        log.info(info)

        # set up scheduler if needed
        self.sched_type = None
        if self.opts.optim.sched:
            sched_type = self.opts.optim.sched.type
            sched_kwargs = {k:v for k,v in self.opts.optim.sched.items() if k != "type"}
            info = f"  * {sched_type} scheduler"
            if sched_type == 'OneCycleLR':  # set additional param accordingly
                assert hasattr(self, 'train_loader'), "Must initialize the training data, to calculate total steps for OneCycleLR"
                steps_per_epoch = len(self.train_loader) // (self.opts.batch_size // len(self.opts.gpu_ids))
                sched_kwargs.update(dict(epochs=self.opts.max_epoch, steps_per_epoch=steps_per_epoch, max_lr=lr_lists))

            self.sched_type = sched_type
            self.sched = getattr(torch.optim.lr_scheduler, sched_type)(self.optim, **sched_kwargs)
            info = info + ' (' + ', '.join([f'{k}={v}' for k, v in sched_kwargs.items()]) + ')'
            log.info(info)

    def restore_checkpoint(self):
        epoch_start, iter_start = 0, 0
        if self.opts.resume:
            log.info("resuming from previous checkpoint...")
            ckpt_path = os.path.join(self.opts.output_path, 'models', 'latest.pth')
            if not os.path.isfile(ckpt_path):
                log.warn(f"can NOT find previous checkpoints at {ckpt_path}")
                log.warn("start training from scratch.")
            else:
                optims_scheds = {x: getattr(self, x) for x in ['optim', 'sched'] if hasattr(self, x)}
                epoch_start, iter_start = utils.restore_checkpoint(self.model, ckpt_path=ckpt_path,
                                                                device=self.opts.device, log=log, resume=True,
                                                                optims_scheds=optims_scheds)
        elif self.opts.load is not None:
            log.info("loading weights from checkpoint {}...".format(self.opts.load))
            epoch_start, iter_start = utils.restore_checkpoint(self.model, ckpt_path=self.opts.load, device=self.opts.device, log=log)
        else:
            log.info("initializing weights from scratch...")
        self.epoch_start = epoch_start or 0
        self.iter_start = iter_start or 0

    def setup_visualizer(self):
        log.info("setting up visualizers...")
        if self.opts.tb:
            from torch.utils import tensorboard
            self.tb = tensorboard.SummaryWriter(log_dir=self.opts.output_path, flush_secs=10)

    def train_model(self):
        # before training
        log.title("TRAINING START")
        self.send_results("TRAINING START", log_msg=False)
        self.timer = edict(start=time.time(), it_mean=None)
        self.it = self.iter_start
        self.val_it = math.ceil(self.opts.freq.val_it * len(self.train_loader)) if self.opts.freq.val_it > 0 else self.opts.freq.val_it
        self.ckpt_it = math.ceil(self.opts.freq.ckpt_it * len(self.train_loader)) if self.opts.freq.ckpt_it > 0 else self.opts.freq.ckpt_it

        # training
        if getattr(self.opts, "sanity_check", False) and self.it == 0:
            self.validate_model(iter=self.it)
            self.test_model(ep=0, send_log=False, save_images=False)

        for self.ep in range(self.epoch_start, self.opts.max_epoch):
            self.train_epoch()

        # after training
        if self.opts.tb:
            self.tb.flush()
            self.tb.close()
        log.title("TRAINING DONE")
        self.send_results("TRAINING DONE", reset_status=True, log_msg=False)

    def train_epoch(self):
        # before train epoch
        self.model.train()
        # train epoch
        tqdm_bar = tqdm.tqdm(self.train_loader, desc="training epoch {}".format(self.ep + 1), leave=False)
        for batch_idx, batch in enumerate(tqdm_bar):
            # train iteration
            if self.opts.resume and self.ep * len(self.train_loader) + batch_idx < self.iter_start:
                continue

            var = edict(batch)
            var = utils.move_to_device(var, self.opts.device)
            loss = self.train_iteration(var)
            tqdm_bar.set_postfix(it=self.it, loss="{:.3f}".format(loss.all))
            
            if self.sched_type == 'OneCycleLR':
                self.sched.step()
            
        # after train epoch
        lr_dict = self.get_cur_lrates()
        log.loss_train(self.opts, self.ep+1, lr_dict, loss.all, self.timer)

        if self.sched_type is not None and self.sched_type != 'OneCycleLR':
            self.sched.step()
        if self.opts.freq.test_ep > 0 and (self.ep + 1) % self.opts.freq.test_ep == 0:
            self.test_model(ep=self.ep+1, send_log=True, save_images=False)
        if self.opts.freq.ckpt_ep > 0 and (self.ep + 1) % self.opts.freq.ckpt_ep == 0: 
            self.save_checkpoint(ep=self.ep+1, it=self.it, backup_ckpt=True)

    def train_iteration(self, var):
        # before train iteration
        self.timer.it_start = time.time()

        # train iteration
        self.optim.zero_grad()
        var_pred = self.model(var, mode="train")
        loss = self.compute_loss(var_pred, var, mode="train")
        loss = summarize_loss(loss, self.opts.loss_weight)
        loss.all.backward()
        if self.opts.optim.clip_enc is not None:
            torch.nn.utils.clip_grad_norm_(self.model.feat_enc.parameters(), self.opts.optim.clip_enc)
        self.optim.step()

        # after train iteration
        self.it += 1
        self.timer.it_end = time.time()
        utils.update_timer(self.opts, self.timer, self.ep, len(self.train_loader))
        if self.opts.freq.scalar > 0 and self.it % self.opts.freq.scalar == 0:
            cur_lrates = self.get_cur_lrates()
            self.log_scalars(loss, self.opts.loss_weight, lrates=cur_lrates, step=self.it, split="train")
        if self.ckpt_it > 0 and self.it % self.ckpt_it == 0:
            self.save_checkpoint(ep=self.ep, it=self.it, backup_ckpt=False)
        if self.val_it > 0 and self.it % self.val_it == 0:
            self.validate_model(iter=self.it)

        return loss

    def compute_loss(self, pred, src, mode=None):
        loss = edict()
        batch_size, n_views, n_chnl = src.images.shape[:3]
        assert n_views == (self.n_src_views + 1), "Make sure the last views are provided as the GT target view"
        target_gt = src.images[:, -1].reshape(batch_size, n_chnl, -1).permute(0, 2, 1)  # (b, h*w, 3)
        if getattr(self.opts.nerf, f"rand_rays_{mode}") and mode in ["train", "test-optim"]:
            target_gt = target_gt[:, pred.ray_idx]
        # compute image losses
        if self.opts.loss_weight.render is not None:
            loss.render = self.MSE_loss(pred.rgb, target_gt)
        if self.opts.loss_weight.render_fine is not None:
            assert(self.opts.nerf.fine_sampling)
            loss.render_fine = self.MSE_loss(pred.rgb_fine, target_gt)
        return loss

    @torch.no_grad()
    def log_scalars(self, loss=None, loss_weight=None, metric=None, lrates=None, step=0, split="train"):
        if loss is not None:
            for key,value in loss.items():
                if key=="all": continue
                if loss_weight[key] is not None:
                    self.tb.add_scalar("{0}/loss_{1}".format(split, key), value, step)
        if metric is not None:
            for key,value in metric.items():
                mean_value = np.array(value).mean()
                self.tb.add_scalar("{0}/{1}".format(split, key), mean_value, step)
        if lrates is not None:
            for key, value in lrates.items():
                self.tb.add_scalar("{0}/{1}".format('lrate', key), value, step)

    @torch.no_grad()
    def get_cur_lrates(self):
        if self.opts.optim.sched:
            lr_enc, lr_dec = self.sched.get_last_lr()[:2]
        else:
            lr_enc = self.opts.optim.lr_enc
            lr_dec = self.opts.optim.lr_dec
        lr_dict = dict(enc=lr_enc, dec=lr_dec)
        if self.opts.nerf.fine_sampling:
            lr_dict['dec_fine'] = lr_dec
        return lr_dict

    @torch.no_grad()
    def visualize(self, var, step=0, split="train"):
        raise NotImplementedError

    def save_checkpoint(self, ep=0, it=0, backup_ckpt=True):
        save_train_info = True

        checkpoint = dict(model=self.model.state_dict())
        if save_train_info:
            train_info = dict(optim=self.optim.state_dict())
            if self.sched_type is not None:
                train_info.update(dict(sched=self.sched.state_dict()))
            checkpoint.update(train_info)

        utils.save_checkpoint(self.opts.output_path, checkpoint, ep=ep, it=it, backup_ckpt=backup_ckpt)

    def send_results(self, msg, reset_status=False, log_msg=True):
        if hasattr(self, "tg_trainnet_bot"):
            if reset_status:
                self.tg_trainnet_bot.reset_msg()
            # first message, append machine and experiment name
            if self.tg_trainnet_bot.msg_id is None and len(self.tg_trainnet_bot.msg_text) == 0:
                header = '<b>#%s #%s</b>\n<b>%s</b>, ' % (socket.gethostname(), self.opts.name, time.strftime("%m%d-%H:%M"))
            else:
                header = '<b>%s</b>, ' % (time.strftime("%m%d-%H:%M"))
            msg = header + msg
            self.tg_trainnet_bot(msg)
        if log_msg:
            log.metric_test(re.sub('<[^<]+?>', '', msg.split('\n')[-1]))

    @torch.no_grad()
    def validate_model(self, iter=None):
        assert hasattr(self, 'val_loader'), "please load validation dataset."
        self.model.eval()
        data_outdir = os.path.join(self.opts.output_path, 'validation')
        os.makedirs(data_outdir, exist_ok=True)
        eval_tools = EvalTools(device=self.opts.device)
        metrics_dict = {k:[] for k in eval_tools.support_metrics}

        tqdm_loader = tqdm.tqdm(self.val_loader, desc="validating", leave=False)
        for batch_idx, batch in enumerate(tqdm_loader):
            if iter == 0 and batch_idx > 0:
                break
            var = edict(batch)
            batch_size = var.images.shape[0]
            var = utils.move_to_device(var, self.opts.device)
            var_pred = self.model(var, mode="val")

            # save image
            img_hw = batch['img_wh'][0].numpy().tolist()[::-1]
            pred_rgb = var_pred['rgb'].reshape(batch_size, *img_hw, -1)
            for batch_idx, cur_rgb in enumerate(pred_rgb):
                pred_rgb_nb = (cur_rgb.detach().cpu().numpy() * 255).astype('uint8')
                out_name = f"{batch['scene'][batch_idx]}_view{batch['view_ids'][batch_idx][-1]}_it{iter}.png"
                imageio.imwrite(os.path.join(data_outdir, out_name), pred_rgb_nb)

            for batch_idx, cur_rgb in enumerate(pred_rgb):
                pred_rgb_nb = cur_rgb.detach().cpu().numpy()
                gt_rgb_nb = var.images[batch_idx, -1].permute(1, 2, 0).detach().cpu().numpy()  # h,w,3
                assert 'depth' in batch, "Must provide 'depth' of target view for validation"
                depth = batch['depth'][batch_idx].detach().cpu().numpy()
                image_mask = depth==0
                eval_tools.set_inputs(pred_rgb_nb, gt_rgb_nb, image_mask)
                cur_metrics = eval_tools.get_metrics()
                for k, v in cur_metrics.items():
                    metrics_dict[k].append(v)

        self.log_scalars(metric=metrics_dict, step=iter, split="val")
        self.model.train()

    @torch.no_grad()
    def test_model(self, ep=None, send_log=True, save_images=True, leave_tqdm=False):
        assert hasattr(self, 'test_loaders'), "Must load the test data for testing."
        test_outroot = os.path.join(self.opts.output_path, 'test')
        os.makedirs(test_outroot, exist_ok=True)
        eval_tools = EvalTools(device=self.opts.device)
        metrics_dict = {}

        self.model.eval()
        for data_loader in self.test_loaders:
            dataname = data_loader.dataset.get_name()
            metrics_dict[dataname] = OrderedDict()
            data_outdir = os.path.join(test_outroot, dataname)
            os.makedirs(data_outdir, exist_ok=True)
            if dataname == 'blender':
                self.model.nerf_setbg_opaque = True

            tqdm_desc = f"testing {dataname}" if ep is None else f"testing {dataname} [epoch {ep}]"
            for batch_idx, batch in enumerate(tqdm.tqdm(data_loader, desc=tqdm_desc, leave=leave_tqdm)):
                if hasattr(self, "it") and self.it == 0 and batch_idx > 0:
                    break

                var = edict(batch)
                var = utils.move_to_device(var, self.opts.device)
                var = self.model(var, mode="test")

                # save image
                batch_size = var['images'].shape[0]
                img_hw = batch['img_wh'][0].numpy().tolist()[::-1]
                pred_rgb = var['rgb'].reshape(batch_size, *img_hw, -1)
                if save_images:
                    for batch_idx, cur_rgb in enumerate(pred_rgb):
                        pred_rgb_nb = (cur_rgb.detach().cpu().numpy() * 255).astype('uint8')
                        src_ids_str = '_'.join([f'{x:02d}' for x in batch['view_ids'][batch_idx][:self.n_src_views]])
                        out_name = f"{batch['scene'][batch_idx]}_view{batch['view_ids'][batch_idx][-1]:02d}_src{src_ids_str}.png"
                        if ep is not None:
                            out_name = f"ep{ep}_{out_name}"
                        imageio.imwrite(os.path.join(data_outdir, out_name), pred_rgb_nb)

                # log metric
                for batch_idx, cur_rgb in enumerate(pred_rgb):
                    pred_rgb_nb = cur_rgb.detach().cpu().numpy()
                    gt_rgb_nb = var.images[batch_idx, -1].permute(1, 2, 0).detach().cpu().numpy()  # h,w,3
                    if 'depth' in batch:
                        depth = batch['depth'][batch_idx].detach().cpu().numpy()
                        image_mask = depth==0
                    else:
                        image_mask = None
                    eval_tools.set_inputs(pred_rgb_nb, gt_rgb_nb, image_mask)
                    cur_metrics = eval_tools.get_metrics()
                    metrics_dict[dataname][f"{var.scene[batch_idx]}_{var.view_ids[batch_idx, -1]:03d}"] = cur_metrics
                    # print(f"{var.scene[batch_idx]}_{var.view_ids[batch_idx, -1]:03d}", cur_metrics)
            # reset params
            self.model.nerf_setbg_opaque = False
        sum_dict = summarize_metrics(metrics_dict, test_outroot, ep=ep)
        if send_log:
            tg_msg = f"{self.ep:02d},{self.it:06d};" if hasattr(self, 'ep') and hasattr(self, 'it') else ""
            for cur_dataname, cur_datametric in sum_dict.items():
                metric_avg = {k: np.array(v).mean() for k, v in cur_datametric.items()}
                tg_msg = tg_msg + f" {cur_dataname.upper()[0]}: {metric_avg['PSNR']:.2f}, {metric_avg['SSIM']:.3f}, {metric_avg['LPIPS']:.3f},"
                if hasattr(self, 'tb'):
                    self.log_scalars(metric=metric_avg, step=ep, split=cur_dataname)
            self.send_results(tg_msg)
        self.model.train()

    @torch.no_grad()
    def test_model_video(self, ep=None, leave_tqdm=False):
        assert hasattr(self, 'test_loaders'), "Must load the test data for testing."
        test_outroot = os.path.join(self.opts.output_path, 'test_videos')
        os.makedirs(test_outroot, exist_ok=True)

        self.model.eval()
        for data_loader in self.test_loaders:
            dataname = data_loader.dataset.get_name()
            data_outdir = os.path.join(test_outroot, dataname)
            os.makedirs(data_outdir, exist_ok=True)

            # set rendering parameters
            if dataname == 'dtu':
                self.model.nerf_setbg_opaque = False
                render_path_mode = 'interpolate'
            elif dataname == 'blender':
                self.model.nerf_setbg_opaque = True
                render_path_mode = 'interpolate'
            elif dataname == 'llff':
                self.model.nerf_setbg_opaque = False
                render_path_mode = 'spiral'
            else:
                raise Exception(f"Unknown dataset for rendering video {dataname}")

            tqdm_desc = f"testing {dataname}" if ep is None else f"testing {dataname} [epoch {ep}]"
            for batch in tqdm.tqdm(data_loader, desc=tqdm_desc, leave=leave_tqdm):
                var = edict(batch)
                var = utils.move_to_device(var, self.opts.device)
                var = self.model(var, mode="test",
                            render_video=self.opts.nerf.render_video, render_path_mode=render_path_mode)

                # save videos
                batch_size = var['images'].shape[0]
                img_hw = batch['img_wh'][0].numpy().tolist()[::-1]
                pred_rgb = var['rgb'].reshape(batch_size, self.opts.nerf.video_n_frames, *img_hw, -1)
                for batch_idx, cur_rgb in enumerate(pred_rgb):
                    pred_rgb_nb = (cur_rgb.detach().cpu().numpy() * 255).astype('uint8')
                    src_ids_str = '_'.join([f'{x:02d}' for x in batch['view_ids'][batch_idx][:self.n_src_views]])
                    out_name = f"{batch['scene'][batch_idx]}_view{batch['view_ids'][batch_idx][-1]:02d}_src{src_ids_str}.mp4"
                    if ep is not None:
                        out_name = f"ep{ep}_{out_name}"
                    pred_rgb_nb_list = [pred_rgb_nb[x] for x in range(pred_rgb_nb.shape[0])]
                    utils.write_video(os.path.join(data_outdir, out_name), pred_rgb_nb_list)

        self.model.train()
