import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from .rfdecoder.ray_transformer import MultiHeadAttention
from .ray_transformer import RayTransformer

from models.embedder import Embedding

from typing import List, Tuple, Union, Optional, Iterator, Dict, Callable

import pdb
import random

torch._C._jit_set_profiling_executor(False)
torch._C._jit_set_profiling_mode(False)


@torch.jit.script
def fused_mean_variance(x, weight):
    mean = torch.sum(x * weight, dim=1, keepdim=True)
    var = torch.sum(weight * (x - mean) ** 2, dim=1, keepdim=True)
    return mean, var


class LatentSDFLayer(nn.Module):
    def __init__(self,
                 d_in: int = 3,
                 d_out: int = 129,
                 d_hidden: int = 128,
                 n_layers: int = 4,
                 skip_in: List[int] = [4],
                 multires: int = 0,
                 bias: int = 0.5,
                 geometric_init: bool = True,
                 weight_norm: bool = True,
                 activation: str = 'softplus',
                 d_conditional_feature: int = 22,
                 use_second_sdf_network: bool = False,
                 post_transformer: bool = False
                 ):
        super(LatentSDFLayer, self).__init__()

        self.d_conditional_feature = d_conditional_feature
        self.d_in = d_in

        # concat latent code for each layer input excepting the first layer and the last layer
        dims_in = [d_in] + [d_hidden + d_conditional_feature for _ in range(n_layers - 2)] + [d_hidden]
        dims_out = [d_hidden for _ in range(n_layers - 1)] + [d_out]

        self.embed_fn_fine = None

        if multires > 0:
            embed_fn = Embedding(in_channels=d_in, N_freqs=multires)  # * include the input
            self.embed_fn_fine = embed_fn
            dims_in[0] = embed_fn.out_channels

        self.num_layers: int = n_layers
        self.skip_in: List[int] = skip_in

        self.num_layers = n_layers
        self.skip_in = skip_in

        # num_layers - 1 because last layer does not have new inputs
        last_skipped = 0
        for l in range(0, self.num_layers):
            if l in self.skip_in:
                # in_dim = dims_in[l] + dims_in[0]
                # breakpoint()
                if last_skipped == 0:
                    in_dim = dims_in[l] + dims_in[0]
                else:
                    in_dim = dims_in[l] + dims_out[last_skipped]

                last_skipped=l
                # last_skipped = l
            else:
                in_dim = dims_in[l]

            out_dim = dims_out[l]
            lin = nn.Linear(in_dim, out_dim)

            if geometric_init:  # - from IDR code,
                if l == self.num_layers - 2 and (not use_second_sdf_network or post_transformer): 
                    torch.nn.init.normal_(lin.weight, mean=np.sqrt(np.pi) / np.sqrt(in_dim), std=0.0001)
                    torch.nn.init.constant_(lin.bias, -bias)
                    # the channels for latent codes are set to 0
                    torch.nn.init.constant_(lin.weight[:, -d_conditional_feature:], 0.0)
                    torch.nn.init.constant_(lin.bias[-d_conditional_feature:], 0.0)
                    # the sdf channel (first one) is initialized to 0
                    # torch.nn.init.constant_(lin.bias[0], 0.0)
                    # torch.nn.init.constant_(lin.weight[:, 0], 0.0)
                    
                if multires > 0 and l == 0:  # the first layer
                    torch.nn.init.constant_(lin.bias, 0.0)
                    # * the channels for position embeddings are set to 0
                    torch.nn.init.constant_(lin.weight[:, 3:], 0.0)
                    # * the channels for the xyz coordinate (3 channels) for initialized by normal distribution
                    torch.nn.init.normal_(lin.weight[:, :3], 0.0, np.sqrt(2) / np.sqrt(out_dim))
                elif multires > 0 and l in self.skip_in:
                    torch.nn.init.constant_(lin.bias, 0.0)
                    torch.nn.init.normal_(lin.weight, 0.0, np.sqrt(2) / np.sqrt(out_dim))
                    # * the channels for position embeddings (and conditional_feature) are initialized to 0
                    # torch.nn.init.constant_(lin.weight[:, -(dims_in[0] - 3 + d_conditional_feature):], 0.0)
                    torch.nn.init.constant_(lin.weight[:, -(dims_out[last_skipped] - 3 + d_conditional_feature):], 0.0)
                    # the sdf channel (first one) is initialized to 0
                    # torch.nn.init.constant_(lin.weight[:, 0], 0.0)
                elif l == self.num_layers - 1 and (not use_second_sdf_network or post_transformer):  #NOTE: might lead to conflict if last layer is skipped
                    torch.nn.init.normal_(lin.weight, mean=np.sqrt(np.pi) / np.sqrt(in_dim), std=0.0001)
                    torch.nn.init.constant_(lin.bias, -bias)
                else:
                    torch.nn.init.constant_(lin.bias, 0.0)
                    torch.nn.init.normal_(lin.weight, 0.0, np.sqrt(2) / np.sqrt(out_dim))
                    # the channels for latent code are initialized to 0
                    torch.nn.init.constant_(lin.weight[:, -d_conditional_feature:], 0.0)
                    # the sdf channel (first one) is initialized to 0
                    # torch.nn.init.constant_(lin.weight[:, 0], 0.0)

            if weight_norm:
                lin = nn.utils.weight_norm(lin)

            setattr(self, "lin" + str(l), lin)

        if activation == 'softplus':
            self.activation = nn.Softplus(beta=100)
        else:
            assert activation == 'relu'
            self.activation = nn.ReLU()

    def forward(self, inputs, latent):
        if self.embed_fn_fine is not None: # (x, n) -> (x, n*13)
            inputs = self.embed_fn_fine(inputs)     

        #print(inputs.shape, latent.shape)
        x = inputs
        for l in range(0, self.num_layers):
            lin = getattr(self, "lin" + str(l))

            # Add the inputs and their positional encoding this time
            if (l-1) in self.skip_in:
                # breakpoint()
                inputs = x
            # * due to the conditional bias, different from original neus version
            if l in self.skip_in:
                x = torch.cat([x, inputs], 1) / np.sqrt(2)

            # Add the latent code
            if 0 < l < self.num_layers - 1:
                x = torch.cat([x, latent], 1)
            
            x = lin(x)

            if l < self.num_layers - 1:
                x = self.activation(x)

        return x


class SparseSdfNetwork(nn.Module):
    '''
    Coarse-to-fine sparse cost regularization network
    return sparse volume feature for extracting sdf
    '''

    def __init__(self,  
                 ch_in: int = 3, 
                 ch_cond_feature: int = 12,
                 hidden_dim: int = 128, 
                 activation: str = 'softplus',
                 num_sdf_layers: int = 4,
                 multires: int = 6,
                 use_ray_transformer: bool = False,
                 use_second_sdf_network: bool = False,
                 ray_transformer_heads: int = 2,
                 ray_transformer_d_model: int = 3,
                 ray_transformer_dim_feedforward: int = 9,
                 ray_transformer_use_full_layer: bool = True,
                 skip_in: List[int] = [4],
                 switch_grad: int = 50000
                 ):
        super(SparseSdfNetwork, self).__init__()

        # Channels and Activation
        self.ch_in = ch_in
        self.ch_cond = ch_cond_feature
        self.hidden_dim = hidden_dim
        self.multires = multires
        self.skip_in = skip_in 
        self.switch_grad = switch_grad

        if activation == 'softplus':
            self.activation = nn.Softplus(beta=100)
        else:
            assert activation == 'relu'
            self.activation = nn.ReLU()

        self.use_ray_transformer: bool = use_ray_transformer
        self.use_second_sdf_network: bool = use_second_sdf_network
        self.ray_transformer_heads: int = ray_transformer_heads
        self.ray_transformer_d_model: int = ray_transformer_d_model
        self.ray_transformer_dim_feedforward: int = ray_transformer_dim_feedforward

        # Output dim of sdf network
        self.sdf_out: int = 1

        # Ray Transformer
        if self.use_ray_transformer:
            self.sdf_out: int = self.ray_transformer_d_model
            self.ray_transformer: RayTransformer = RayTransformer(self.ray_transformer_heads,
                                                                  self.ray_transformer_d_model,
                                                                  self.ray_transformer_heads,
                                                                  self.ray_transformer_heads,
                                                                  self.ray_transformer_dim_feedforward,
                                                                  use_full_layer=ray_transformer_use_full_layer)
            if self.use_second_sdf_network:
                self.sdf_layer_2 = LatentSDFLayer(d_in=self.ch_in,     
                                                d_out=self.hidden_dim + 1, #+1 for sdf
                                                    d_hidden=self.hidden_dim+1,
                                                    n_layers=num_sdf_layers,
                                                    multires=multires,
                                                    skip_in=skip_in,
                                                    geometric_init=True,
                                                    weight_norm=True,
                                                    activation=activation,
                                                    d_conditional_feature=self.hidden_dim+1,   #+1 for sdf
                                                    use_second_sdf_network=self.use_second_sdf_network,
                                                    post_transformer=True
                                                    )


        # SDF Layer
        self.sdf_layer = LatentSDFLayer(d_in=self.ch_in,
                                        d_out=self.hidden_dim + self.sdf_out,
                                        d_hidden=self.hidden_dim,
                                        n_layers=num_sdf_layers,
                                        multires=multires,
                                        skip_in=skip_in,
                                        geometric_init=True,
                                        weight_norm=True,
                                        activation=activation,
                                        d_conditional_feature=self.ch_cond,
                                        use_second_sdf_network=self.use_second_sdf_network,
                                        post_transformer=False
                                        )
        
    def sdf(self, 
            pts: torch.Tensor, 
            cond_feats: torch.Tensor,
            iter_step: int = 0
            ) -> torch.Tensor:
        """ Gets the output of the sdf layer

        Args:
            pts (torch.Tensor): 3D points to evaluate the SDF at
            cond_feats (torch.Tensor): Conditional features to use for the SDF

        Returns:
            torch.Tensor: SDF outputs at the given points
        """
        num_pts = pts.shape[0]
        device = pts.device
        # pts_ = pts.clone()

        sampled_feats = cond_feats        #naming sampled_feature is not too correct I think, needs some rework
        
        if self.use_ray_transformer:
            if len(sampled_feats.shape) == 3:
                NR, NP, _ = sampled_feats.shape
            else:
                if len(sampled_feats.shape) == 2:
                    raise ValueError("Only support dim = 3 or 4")
                B, NR, NP, _ = sampled_feats.shape
                assert B == 1, "Only support batch size 1 for now"
        
        sampled_feats = sampled_feats.view(num_pts, -1).contiguous().to(device)
        # breakpoint()
        # Get SDF value
        # sdf_pts = self.sdf_layer(pts_, sampled_feats)
        sdf_pts = self.sdf_layer(pts, sampled_feats)

        outputs = {}
        outputs['sdf_pts'] = sdf_pts[:, :self.sdf_out]

        if self.use_ray_transformer:
            inp_transformer = outputs['sdf_pts'].reshape(NR, NP, self.sdf_out) # (NR, NP, self.sdf_out)
            out_transformer = self.ray_transformer(inp_transformer) # (NR, NP, 1)
            outputs['sdf_pts'] = out_transformer.reshape(-1, 1) # (NR*NP, 1)
            if self.use_second_sdf_network:
                outputs['sdf_features_pts'] = sdf_pts[:, self.sdf_out:]
                input_to_sdf_2 = torch.cat([outputs['sdf_pts'], outputs['sdf_features_pts']], dim=-1)
                sdf_pts_2 = self.sdf_layer_2(pts, input_to_sdf_2)
                outputs['sdf_pts'] = sdf_pts_2[:, :1]
                outputs['sdf_features_pts'] = sdf_pts_2[:, 1:]
                outputs['sampled_latent'] = input_to_sdf_2
                return outputs
        
        outputs['sdf_features_pts'] = sdf_pts[:, self.sdf_out:]
        outputs['sampled_latent'] = sampled_feats

        return outputs

    def gradient(self, 
                 x: torch.Tensor, 
                 ref_poses: Dict[str, torch.Tensor],
                 ref_feats_list: List[torch.Tensor],
                 imgs: torch.Tensor,
                 query_func: Callable,
                 cond_feat_query_func: Callable,
                 iter_step: int = 0
                 ) -> torch.Tensor:
        """ Return the gradient of the SDF at the given points.

        Args:
            x (torch.Tensor): Points in 3D
            cond_feats (torch.Tensor): Conditional features

        Returns:
            torch.Tensor: Gradients of the SDF at the given points
        """
        # cond_feats.requires_grad_(True)

        # breakpoint()
        #To speed up process, we can use the gradient of previous model at first
        if(iter_step < self.switch_grad):
            return self.gradient_prev(x, ref_poses, imgs, ref_feats_list, x.device, query_func, cond_feat_query_func)
        
        x.requires_grad_(True)
        #TODO: Fix!!!

        # breakpoint()
        cond_info = query_func(x[None,...], ref_poses, imgs, ref_feats_list, x.device)
        # cond_feats = torch.cat([cond_info['new_feat'], cond_info['feat_info'], cond_info['mask_info']], dim=-1)
        
        cond_feats = cond_feat_query_func(cond_info) 
    
        pts_mask_bool = (cond_info['mask_info'][0].sum(-1) > cond_info['threshold'])

        # If we use ray similarity transformer, we should update the whole sdf volume if any of the points is inside frustum mask
        if self.use_ray_transformer:
            # If all points are outside frustum mask, we still take a single ray to update sdf volume
            if torch.sum(pts_mask_bool) < 1:
                pts_mask_bool[0] = True
            # check if any rays are completely outside and don't update these but update all other rays completely
            pts_mask_bool = (torch.sum(pts_mask_bool, dim=1) > 1).reshape(-1,1).repeat(1, x.shape[-2]) # (n_rays, n_samples)
            sampled_feats = cond_feats[pts_mask_bool[:,0].unsqueeze(0)] # N_rays_sampled * n_samples, C_latent
            output = self.sdf(x[pts_mask_bool], sampled_feats.unsqueeze(0))
        else:
            if torch.sum(pts_mask_bool.float()) < 1:  # ! when render out image, may meet this problem
                pts_mask_bool[:100] = True
            
            output = self.sdf(x[pts_mask_bool], cond_feats[pts_mask_bool.unsqueeze(0)])

        y = output['sdf_pts']
        
        # if(iter_step < self.switch_grad):
        #     x = pts
        # breakpoint()
        d_output = torch.ones_like(y, requires_grad=False, device=y.device)
        # ! Distributed Data Parallel doesn’t work with torch.autograd.grad()
        # ! (i.e. it will only work if gradients are to be accumulated in .grad attributes of parameters).
        gradients = torch.autograd.grad(
            outputs=y,
            inputs=x,
            grad_outputs=d_output,
            create_graph=True,
            retain_graph=True,
            only_inputs=True)
        # breakpoint()
        # Calculate and return gradients wrt both input coordinates and features
        return gradients[0][pts_mask_bool], None
    
    def gradient_prev(self, 
                 x: torch.Tensor, 
                 ref_poses: Dict[str, torch.Tensor],
                 imgs: torch.Tensor,
                 ref_feats_list: List[torch.Tensor],
                 device: torch.device,
                 query_func,
                 cond_feat_query_func
                 ) -> torch.Tensor:
        """ Return the gradient of the SDF at the given points.

        Args:
            x (torch.Tensor): Points in 3D
            cond_feats (torch.Tensor): Conditional features

        Returns:
            torch.Tensor: Gradients of the SDF at the given points
        """
        # cond_feats.requires_grad_(True)

        x.requires_grad_(True)
        
        cond_info = query_func(x[None, ...], ref_poses, imgs, ref_feats_list, device)
        # cond_feats = torch.cat([cond_info['feat_info'], cond_info['color_info'], cond_info['mask_info']], dim=-1)
        # cond_feats = torch.cat([cond_info['new_feat'],cond_info['feat_info'], cond_info['mask_info']], dim=-1)
        cond_feats = cond_feat_query_func(cond_info) 
        pts_mask_bool = (cond_info['mask_info'][0].sum(-1) > cond_info['threshold'])

        # If we use ray similarity transformer, we should update the whole sdf volume if any of the points is inside frustum mask
        if self.use_ray_transformer:
            # If all points are outside frustum mask, we still take a single ray to update sdf volume
            if torch.sum(pts_mask_bool) < 1:
                pts_mask_bool[0] = True
            
            # check if any rays are completely outside and don't update these but update all other rays completely
            pts_mask_bool = (torch.sum(pts_mask_bool, dim=1) > 1).reshape(-1,1).repeat(1, x.shape[-2]) # (n_rays, n_samples)
            sampled_feats = cond_feats[pts_mask_bool[:,0].unsqueeze(0)] # N_rays_sampled * n_samples, C_latent
            output = self.sdf(x[pts_mask_bool], sampled_feats.unsqueeze(0))
        else:
            if torch.sum(pts_mask_bool.float()) < 1:  # ! when render out image, may meet this problem
                pts_mask_bool[:100] = True
            
            output = self.sdf(x[pts_mask_bool], cond_feats[pts_mask_bool.unsqueeze(0)])

        pts_mask_bool_flat = pts_mask_bool.view(-1)
        pts = x.view(-1, 3)

        sampled_feats = cond_feats.view(pts.shape[0], -1)

        output = self.sdf(pts[pts_mask_bool_flat], sampled_feats[pts_mask_bool_flat])
        y = output['sdf_pts']

        # pts = x.reshape(-1, 3)
        # sampled_feats = cond_feats.view(pts.shape[0], -1)

        # output = self.sdf(pts[pts_mask_bool], sampled_feats[pts_mask_bool])
        # y = output['sdf_pts']

        d_output = torch.ones_like(y, requires_grad=False, device=y.device)
        # ! Distributed Data Parallel doesn’t work with torch.autograd.grad()
        # ! (i.e. it will only work if gradients are to be accumulated in .grad attributes of parameters).
        
        gradients = torch.autograd.grad(
            outputs=y,
            inputs=pts,
            grad_outputs=d_output,
            create_graph=True,
            retain_graph=True,
            only_inputs=True)
        
        # Calculate and return gradients wrt both input coordinates and features
        # return gradients[0].view(pts.shape[0], -1), None
        return gradients[0][pts_mask_bool_flat], None