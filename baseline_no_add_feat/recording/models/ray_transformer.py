import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from .rfdecoder.ray_transformer import MultiHeadAttention

from typing import List, Tuple, Union, Optional, Iterator, Dict, Callable


class RayTransformer(nn.Module):
    """ Ray Transformer
            Copied and adapted from MatchNeRF
    """
    def __init__(self, 
                 n_head: int,
                 d_model: int,
                 d_k: int,
                 d_v: int,
                 dim_feedforward: int,
                 activation: str = 'relu',
                 activate_first: bool = True,
                 dropout: float = 0.1,
                 use_full_layer: bool = True,
                 ) -> None:
        super().__init__()

        self.use_full_layer = use_full_layer

        self.multi_head_attention = MultiHeadAttention(n_head, d_model, d_k, d_v)

        # Tranformer Feed Forward Layer
        self.expand_linear = nn.Linear(d_model, dim_feedforward)
        self.dropout = nn.Dropout(dropout)

        # If full layer: Use dim_feedforward + residual connection + layer norm, otherwise use only dim_feedforward -> output 1D
        if self.use_full_layer:
            self.compress_linear = nn.Linear(dim_feedforward, d_model)
            self.norm = nn.LayerNorm(d_model, eps=1e-6)

            # Output Layer to get the final 1D output which is the SDF
            self.out_linear = nn.Linear(d_model, 1)
        else:
            # Old behavior
            self.compress_linear = nn.Linear(dim_feedforward, 1)

        # What type of activation to use
        self.activation = self.get_activation(activation)

        # Whether to apply activation before the multi-head attention
        self.activate_first = activate_first

    def get_activation(self, activation: str) -> nn.Module:
        if activation == 'relu':
            return nn.ReLU()
        elif activation == 'tanh':
            return nn.Tanh()
        elif activation == 'sigmoid':
            return nn.Sigmoid()
        else:
            raise NotImplementedError(f'Not supported activation: {activation}')

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        assert x.dim() == 3
        x = self.activation(x) if self.activate_first else x
        res, _ = self.multi_head_attention(x, x, x)

        if self.use_full_layer:
            # First expand, then compress while always applying activation
            x = self.activation(self.compress_linear(self.dropout(self.activation(self.expand_linear(res)))))

            # Residual connection plus layer norm
            x = self.norm(x + res)

            out = self.out_linear(x)
        else:
            # Old behavior
            out = self.compress_linear(self.dropout(self.activation(self.expand_linear(res))))
        return out
