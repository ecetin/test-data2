from .matchnerf import MatchNeRF
from .matchnerf_sparse import MatchNeRFSparse

models_dict = {
    'matchnerf': MatchNeRF,
    'matchnerf_sparse': MatchNeRFSparse,
}
