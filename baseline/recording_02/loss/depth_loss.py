import torch
import torch.nn as nn
import torch.nn.functional as F


class DepthLoss(nn.Module):
    def __init__(self, type='l1'):
        super(DepthLoss, self).__init__()
        self.type = type
        self.fct = nn.L1Loss()

    def forward(self, depth_pred, depth_gt, mask):
        mask_d = depth_gt > 0

        mask = (mask * mask_d).bool()
        
        depth_pred = depth_pred[mask]
        depth_gt = depth_gt[mask]

        depth_loss = self.fct(depth_pred, depth_gt)

        return depth_loss


class DepthSmoothLoss(nn.Module):
    def __init__(self):
        super(DepthSmoothLoss, self).__init__()

    def forward(self, disp, img, mask):
        """
        Computes the smoothness loss for a disparity image
        The color image is used for edge-aware smoothness
        :param disp: [B, 1, H, W]
        :param img: [B, 1, H, W]
        :param mask: [B, 1, H, W]
        :return:
        """
        grad_disp_x = torch.abs(disp[:, :, :, :-1] - disp[:, :, :, 1:])
        grad_disp_y = torch.abs(disp[:, :, :-1, :] - disp[:, :, 1:, :])

        grad_img_x = torch.mean(torch.abs(img[:, :, :, :-1] - img[:, :, :, 1:]), 1, keepdim=True)
        grad_img_y = torch.mean(torch.abs(img[:, :, :-1, :] - img[:, :, 1:, :]), 1, keepdim=True)

        grad_disp_x *= torch.exp(-grad_img_x)
        grad_disp_y *= torch.exp(-grad_img_y)

        grad_disp = (grad_disp_x * mask[:, :, :, :-1]).mean() + (grad_disp_y * mask[:, :, :-1, :]).mean()

        return grad_disp
